
# Sublime Text 4 default theme override

This is a simple override for Sublime Text's default adaptive theme. It has to be cloned/unpacked directly into **Packages** folder with the name **Theme - Default**.

- File icons from [A File Icon](https://packagecontrol.io/packages/A%20File%20Icon) plugin.
    - **file_type_python** and **file_type_cython**: The Python logo is blue and yellow and that is sacred. LOL
    - **file_type_js**, **file_type_restructuredtext**, **file_type_markdown**: Too much details for so little an icon. So I removed the icons *frames* and left only the letters.
    - **file_type_json**: It used the database icon, an icon reused several times. I changed it for a couple of curly brackets icon.
- Overridden folder icons with arrows. Default arrows hidden.
- Overridden RegEx and highlight icons.
- Made all icons in sidebar opaque (without transparency).
- Less padding in sidebar elements.
- Made all icons square, not rectangular.
- Unselected tabs labels darker for more contrast and with hover effect without animations.
- Selected tabs labels always bold.
- Added hover style to quick panels.
- Changed minimap color.
