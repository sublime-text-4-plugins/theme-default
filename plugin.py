# -*- coding: utf-8 -*-
from .plugins import *  # noqa
from .plugins import events


def plugin_loaded():
    """On plugin loaded callback."""
    events.broadcast("plugin_loaded")


def plugin_unloaded():
    """On plugin unloaded."""
    events.broadcast("plugin_unloaded")


if __name__ == "__main__":
    pass
