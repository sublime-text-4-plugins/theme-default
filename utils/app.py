#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Development utilities.

Attributes
----------
docopt_doc : str
    Used to store/define the docstring that will be passed to ``docopt`` as the ``doc`` argument.
root_folder : str
    The main folder containing the application.
sublime_text_config_folder : str
    Path to Sublime Text configuration folder.
sublime_text_lib_folder : str
    Path to Sublime Text Lib folder inside the configuration folder.
"""
import os
import sys

root_folder = os.path.realpath(
    os.path.abspath(
        os.path.join(
            os.path.normpath(os.path.join(os.path.dirname(__file__), os.pardir))
        )
    )
)

sublime_text_config_folder = os.path.normpath(
    os.path.join(root_folder, *([os.pardir] * 2))
)
sublime_text_lib_folder = os.path.join(sublime_text_config_folder, "Lib", "python38")

sys.path.insert(0, sublime_text_lib_folder)
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))

from python_utils import yaml_utils
from python_utils.docopt import docopt

from icons_builder import check_source_icons
from icons_builder import create_icons
from icons_builder import create_preferences
from icons_builder import info

docopt_doc = """Theme - Default overrides development utilities

Usage:
    app.py (-h | --help | --version)
    app.py create_icons       [--overwrite]
    app.py create_preferences [--overwrite]
    app.py check_source_icons

Options:

-h, --help
    Show this application basic help.

--version
    Show application version.

--overwrite
    Overwrite existent files when the **create_icons** or **create_preferences**
    commands are executed.

Commands:

create_icons
    Create all **PNG** icons from **SVG** sources.

create_preferences
    Create all the .tmPreferences files.

check_source_icons
    Check if all SVG icons are definied in the **icons.yaml** file and also
    check if all icons in the **icons.yaml** file have their corresponding
    SVG file.

"""


if __name__ == "__main__":
    if len(sys.argv) == 1:
        sys.argv.append("--help")

    args = docopt(docopt_doc, version="Ω")
    init_icons = any(
        (
            args.get("create_preferences"),
            args.get("create_icons"),
            args.get("check_source_icons"),
        )
    )
    init_colors = any((args.get("create_icons"),))

    if init_icons:
        with open(os.path.join(root_folder, "utils", "icons.yaml")) as fp:
            icons = yaml_utils.load(fp)

    if init_colors:
        with open(os.path.join(root_folder, "utils", "colors.yaml")) as fp:
            colors = yaml_utils.load(fp)

    if args.get("create_preferences"):
        info("Creating preference files...")
        create_preferences(icons, overwrite=args.get("--overwrite"))

    if args.get("check_source_icons"):
        info("Checking source icons...")
        check_source_icons(icons)

    if args.get("create_icons"):
        info("Creating PNG icons...")
        create_icons(icons, colors, overwrite=args.get("--overwrite"))
