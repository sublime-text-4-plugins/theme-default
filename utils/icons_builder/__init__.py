# -*- coding: utf-8 -*-
"""Summary

Attributes
----------
blacks : tuple
    List of black colors.
root_folder : str
    The main folder containing the application.
"""
import os

import xml.etree.ElementTree as ET

from textwrap import dedent


try:
    import cairosvg

    cairosvg_found = True
except ImportError:
    cairosvg_found = False

from python_utils import pypng
from python_utils.ansi_colors import colorize
from python_utils.colour import Color

root_folder = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
blacks = ("#000", "#000000")

_simple_svg_doc_template = """<svg viewBox="0 0 16 16" version="1.1" xmlns="http://www.w3.org/2000/svg">
{svg_children}
</svg>
"""


def info(msg):
    """Info message.

    Parameters
    ----------
    msg : str
        Message.
    """
    print(colorize(msg, "magenta"))


def success(msg):
    """Success message.

    Parameters
    ----------
    msg : str
        Message
    """
    print(colorize(msg, "success"))


def warning(msg):
    """Warning message.

    Parameters
    ----------
    msg : str
        Message
    """
    print(colorize(msg, "warning"))


def error(msg):
    """Error message.

    Parameters
    ----------
    msg : str
        Message
    """
    print(colorize(msg, "error"))


def check_source_icons(icons):
    """Check source icons.

    Parameters
    ----------
    icons : dict
        Icons database.
    """
    svg_files_names = set(
        [
            entry.name[:-4]
            for entry in os.scandir(os.path.join(root_folder, "utils", "svg_icons"))
            if entry.name.endswith(".svg")
        ]
    )
    database_names = set(icons.keys())
    missing_database_icons = list(database_names.difference(svg_files_names))
    missing_svg_icons = list(svg_files_names.difference(database_names))

    if missing_database_icons:
        warning(
            "Found SVG %d icons not defined in the database:"
            % len(missing_database_icons)
        )
        warning("\n".join(missing_database_icons))
    else:
        success("The icons database has no missing icons.")

    if missing_svg_icons:
        warning(
            "Found %d icon/s defined in the database without its/their corresponding SVG file:"
            % len(missing_svg_icons)
        )
        warning("\n".join(missing_svg_icons))
    else:
        success("All SVG icons are definied in the icons database.")


def create_preferences(icons, overwrite=False):
    """Create .tmPreferences files.

    Parameters
    ----------
    icons : dict
        Icons database.
    overwrite : bool, optional
        Overwrite existent files.
    """
    os.makedirs(os.path.join(root_folder, "preferences"), exist_ok=True)
    template = dedent(
        """
        <?xml version="1.0" encoding="UTF-8"?>
        <plist version="1.0">
            <dict>
                <key>scope</key>
                <string>{scope}</string>
                <key>settings</key>
                <dict>
                    <key>icon</key>
                    <string>{name}</string>
                </dict>
            </dict>
        </plist>
        """
    ).lstrip()

    for name, data in icons.items():
        go_ahead = True
        scopes = set()
        for keys in ("aliases", "syntaxes"):
            for syntax in data.get(keys, []):
                for scope in syntax["scope"].split(","):
                    scopes.add(scope.strip())

        pref_path = os.path.join(root_folder, "preferences", name + ".tmPreferences")

        if not overwrite and os.path.exists(pref_path):
            go_ahead = False

        if scopes and go_ahead:
            with open(pref_path, "w") as out:
                out.write(template.format(name=name, scope=", ".join(sorted(scopes))))


def create_png(bytestring, write_to, size):
    """Create PNG file from SVG.

    Parameters
    ----------
    bytestring : str
        SVG file content.
    write_to : str
        Path to PNG file.
    size : int
        PNG image size.
    """
    width, height, rows, info = pypng.Reader(
        bytes=cairosvg.svg2png(
            bytestring=bytestring, parent_height=size, parent_width=size
        )
    ).asRGBA()
    with open(write_to, "wb") as fp:
        pypng.Writer(compression=9, **info).write(fp, rows)


def register_namespaces():
    """Register XML namespaces."""
    ET.register_namespace("", "http://www.w3.org/2000/svg")
    ET.register_namespace("cc", "http://creativecommons.org/ns#")
    ET.register_namespace("dc", "http://purl.org/dc/elements/1.1/")
    ET.register_namespace("inkscape", "http://www.inkscape.org/namespaces/inkscape")
    ET.register_namespace("rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#")
    ET.register_namespace(
        "sodipodi", "http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
    )
    ET.register_namespace("xlink", "http://www.w3.org/1999/xlink")


def modify_color_luminance(primary_hex, secondary_hex):
    """Modify color luminance.

    Parameters
    ----------
    primary_hex : strip
        The color to modify.
    secondary_hex : strip
        The color from wich to get the luminance.

    Returns
    -------
    str
        Modified color.
    """
    primary_hex = Color(color=primary_hex)
    secondary_hex = Color(color=secondary_hex)
    primary_hex.set_luminance(secondary_hex.get_luminance())

    return str(primary_hex)


def colorize_icon(svg_file, new_color):
    """Colorize icon.

    Parameters
    ----------
    svg_file : str
        SVG file content.
    new_color : str
        The new color.

    Returns
    -------
    str
        New SVG content.
    """
    register_namespaces()
    svg_xml = ET.parse(svg_file)
    svg_root = svg_xml.getroot()
    svg_paths = svg_root.findall(".//{http://www.w3.org/2000/svg}path")
    new_svg_root = ET.fromstring(_simple_svg_doc_template.format(svg_children=""))
    svg_children = []

    for p in svg_paths:
        new_path = ET.SubElement(new_svg_root, "path")
        for k, v in p.attrib.items():
            new_path.set(k, v)

        old_fill = new_path.get("fill", "no_fill")

        if old_fill in blacks or old_fill == "no_fill":
            # NOTE: If no fill or black, override with new color.
            new_path.set("fill", new_color)
        else:
            # NOTE: If any other color, override with new color with the luminance of the old color.
            new_path.set("fill", modify_color_luminance(new_color, old_fill))

        svg_children.append(
            ET.tostring(
                new_path, encoding="utf-8", method="xml", short_empty_elements=True
            ).decode("utf-8")
        )

    new_svg_doc = _simple_svg_doc_template.format(
        svg_children="\n".join(svg_children).strip()
    )

    return new_svg_doc


def create_icons(icons, colors, overwrite=False):
    """Create icons.

    Parameters
    ----------
    icons : dict
        Icons database.
    colors : dict
        Colors database.
    overwrite : bool, optional
        Overwrite existent icons.
    """
    if not cairosvg_found:
        error(
            "Error: CairoSVG not installed!\n"
            "       Run `pip install cairosvg`!\n"
            "       Windows users need the cairo.dll from"
            " https://github.com/preshing/cairo-windows/releases"
        )
        raise SystemExit(1)

    os.makedirs(os.path.join(root_folder, "icons"), exist_ok=True)
    icons_to_create = {}
    for icon_name, icon_data in icons.items():
        icons_to_create[icon_name] = {
            "png_paths": [],
            "color": colors.get(icon_data.get("color", "none")),
        }

        for size in (1, 2, 3):
            go_ahead = True
            suffix = "@{}x.png".format(size) if size > 1 else ".png"
            png_path = os.path.join(root_folder, "icons", icon_name + suffix)

            if not overwrite and os.path.exists(png_path):
                go_ahead = False

            if go_ahead:
                icons_to_create[icon_name]["png_paths"].append(png_path)

    for icon_name, icon_data in icons_to_create.items():
        if icon_data["png_paths"]:
            with open(
                os.path.join(root_folder, "utils", "svg_icons", icon_name + ".svg"), "r"
            ) as svg_file:
                if icon_data["color"]:
                    svg_raw = colorize_icon(svg_file, icon_data["color"])
                else:
                    svg_raw = svg_file.read()

            for png_path in icon_data["png_paths"]:
                if "@3x.png" in png_path:
                    size = 48
                elif "@2x.png" in png_path:
                    size = 32
                else:
                    size = 16

                create_png(
                    bytestring=svg_raw,
                    write_to=png_path,
                    size=size,
                )


if __name__ == "__main__":
    pass
