# -*- coding: utf-8 -*-
import os
import shutil

from textwrap import dedent

import sublime
import sublime_plugin

from python_utils import logging_system
from python_utils import yaml_utils
from python_utils.sublime_text_utils import settings as settings_utils
from python_utils.sublime_text_utils.events import Events
from python_utils.sublime_text_utils.queue import Queue

queue = Queue()
events = Events()

__all__ = ["OdyseusHandleSyntaxAliasesCommand"]

root_folder = os.path.realpath(
    os.path.abspath(
        os.path.join(
            os.path.normpath(os.path.join(os.path.dirname(__file__), os.pardir))
        )
    )
)
aliases_folder = "{0} {1} {0}".format("zzz", os.path.basename(root_folder))
plugin_name = os.path.basename(root_folder)

logger: logging_system.Logger = logging_system.Logger(
    logger_name=plugin_name,
    use_file_handler=os.path.join(root_folder, "tmp", "logs"),
)
settings = settings_utils.SettingsManager(settings_file=plugin_name, events=events, logger=logger)

EMPTY_TEMPLATE = dedent(
    """
    %YAML 1.2
    ---
    name: {0}
    scope: {1}
    hidden: true
    file_extensions:
        - {2}
    contexts:
        main: []
    """
).lstrip()

MAIN_TEMPLATE = dedent(
    """
    %YAML 1.2
    ---
    name: {0}
    scope: {1}
    hidden: true
    file_extensions:
        - {2}
    contexts:
        main:
            - include: scope:{3}
              apply_prototype: true
    """
).lstrip()


def icons_database():
    with open(os.path.join(root_folder, "utils", "icons.yaml")) as fp:
        icons = yaml_utils.load(fp)

    return icons


def set_logging_level():
    try:
        logger.set_logging_level(logging_level=settings.get("logging_level", "ERROR"))
    except Exception as err:
        print(__file__, err)


@events.on("plugin_loaded")
def on_plugin_loaded():
    """On plugin loaded."""
    queue.debounce(
        settings.load,
        delay=100,
        key=f"{plugin_name}-debounce-settings-load",
    )
    queue.debounce(
        set_logging_level,
        delay=200,
        key=f"{plugin_name}-debounce-set-logging-level",
    )


@events.on("plugin_unloaded")
def on_plugin_unloaded():
    settings.unobserve()
    queue.unload()
    events.destroy()


@events.on("settings_changed")
def on_settings_changed(settings_obj, **kwargs):
    if settings_obj.has_changed("logging_level"):
        queue.debounce(
            set_logging_level,
            delay=1000,
            key=f"{plugin_name}-debounce-settings-changed",
        )


class OdyseusHandleSyntaxAliasesCommand(sublime_plugin.WindowCommand):
    def run(self, toggle=False):
        if toggle:
            remove_aliases()
            sublime.status_message("Aliases will be created in 3 seconds.")
            sublime.set_timeout_async(create_aliases, 3000)
        else:
            sublime.set_timeout_async(remove_aliases, 10)


def cache_path():
    return os.path.join(sublime.cache_path(), aliases_folder)


def aliases_path(file_name=""):
    return os.path.join(sublime.packages_path(), aliases_folder, file_name)


def aliases_resource_path(file_name=""):
    return "Packages/{}/{}".format(aliases_folder, file_name)


def remove_aliases():
    logger.info("Disabling syntax aliases")

    def delete_alias_files(syntaxes):
        base_syntaxes = None
        for syntax in syntaxes:
            base_syntaxes = sublime.find_syntax_by_scope(
                syntax.get("base", "text.plain")
            )
            if base_syntaxes:
                delete_alias_file(syntax, base_syntaxes[0].path)
            else:
                delete_alias_file(syntax, "Plain text.tmLanguage")

    for file_type in icons_database().values():
        delete_alias_files(file_type.get("aliases", []))
        delete_alias_files(file_type.get("syntaxes", []))

    shutil.rmtree(aliases_path(), ignore_errors=True)
    shutil.rmtree(cache_path(), ignore_errors=True)
    sublime.status_message("All aliases were removed.")


def create_aliases():
    # Built a dict of { scope: syntax } from visible/real syntaxes.
    # Note: Existing aliases in the overlay are hidden and thus excluded
    #       by default. Also ignore possible aliases or special purpose
    #       syntaxes from 3rd-party packages.
    real_syntaxes = {s.scope: s.path for s in sublime.list_syntaxes() if not s.hidden}

    def real_syntax_for(selector):
        for scope in selector.split(","):
            real_syntax = real_syntaxes.get(scope.strip())
            if real_syntax:
                return real_syntax
        return None

    def check_alias_files(syntaxes):
        for syntax in syntaxes:
            real_syntax = real_syntax_for(syntax["scope"])
            if real_syntax:
                delete_alias_file(syntax, real_syntax)
            elif "extensions" in syntax:
                create_alias_file(syntax)

    try:
        os.makedirs(aliases_path())
    except FileExistsError:
        logger.info("Updating syntax aliases")
    else:
        logger.info("Enabling syntax aliases")

    for file_type in icons_database().values():
        check_alias_files(file_type.get("aliases", []))
        check_alias_files(file_type.get("syntaxes", []))

    sublime.status_message("All aliases were created.")


def create_alias_file(alias):
    name = alias["name"]
    scope = alias["scope"].split(",", 1)[0]
    exts = "\n    - ".join(alias["extensions"])
    base = alias.get("base")

    alias_path = aliases_path(name + ".sublime-syntax")
    try:
        with open(alias_path, "x", encoding="utf-8") as out:
            if base:
                out.write(MAIN_TEMPLATE.format(name, scope, exts, base))
            else:
                out.write(EMPTY_TEMPLATE.format(name, scope, exts, base))
    except FileExistsError:
        pass
    except Exception as error:
        logger.debug("+ {}.sublime-syntax | {}".format(name, error))
    else:
        logger.debug("+ {}.sublime-syntax".format(name))


def delete_alias_file(alias, real_syntax):
    alias_name = alias["name"] + ".sublime-syntax"
    alias_path = aliases_path(alias_name)
    if not os.path.exists(alias_path):
        return

    # reassign real syntax to any open view, which uses the alias
    alias_resource = aliases_resource_path(alias_name)
    for window in sublime.windows():
        for view in window.views():
            syntax = view.settings().get("syntax")
            if syntax and syntax == alias_resource:
                view.assign_syntax(real_syntax)

    # actually delete the alias syntax
    try:
        os.remove(alias_path)
    except Exception as error:
        logger.debug("- {} | {}".format(alias_name, error))
    else:
        logger.debug("- {}".format(alias_name))


if __name__ == "__main__":
    pass
